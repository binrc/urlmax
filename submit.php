<?php
include "includes/header.html";
function insertURL($sc, $url){
	$count = 1;
	$none = 0;
	// see if it exists;
	$db = new SQlite3("db.sqlite3");
	$st = $db->prepare("select * from links where shortcode = :shortcode");
	$st->bindParam(":shortcode", $sc, SQLITE3_TEXT);
	$res = $st->execute();
	$row = $res->fetchArray();
	$db->close();

	if(!$row){	// insert if not exists
		$db = new SQlite3("db.sqlite3");
		$st = $db->prepare("insert into links(shortcode, url, created, viewed) values(:shortcode,:url, :counter, :viewed);");
		$st->bindParam(":shortcode", $sc, SQLITE3_TEXT);
		$st->bindParam(":url", $url, SQLITE3_TEXT);
		$st->bindParam(":counter", $count, SQLITE3_INTEGER);
		$st->bindParam(":viewed", $none, SQLITE3_INTEGER);
		$st->execute();
		$db->close();
	} else {	// modify if exists
		$count = $row[2] + 1;
		$db = new SQlite3("db.sqlite3");
		$st = $db->prepare("update links set created = :counter where shortcode = :shortcode");
		$st->bindParam(":shortcode", $sc, SQLITE3_TEXT);
		$st->bindParam(":counter", $count, SQLITE3_INTEGER);
		$st->execute();
		$db->close();
	}

	return 0;
};

$url = $_POST["input"];

if(filter_var($url, FILTER_VALIDATE_URL)){
	$sc = hash("sha512", $url);
	$surl = "http://" . $_SERVER["HTTP_HOST"] . "?q=" . $sc;
	$ret = insertURL($sc, $url);
	$randomshit = str_replace("/", "", base64_encode(random_bytes(2000 - strlen($surl))));
	printf("<p><a href='%s'>%s</a> has been lengthened to: </p><p class='bigtext'><a href='%s%s'>%s%s</a></p>", $url, $url, $surl, $randomshit, $surl, $randomshit);
} else {
	echo "$url is not a valid URL";
	include "includes/footer.html";
	header( "Refresh:3; url=http://" . $_SERVER["HTTP_HOST"] . "/index.php");
}
	include "includes/footer.html";
?>
