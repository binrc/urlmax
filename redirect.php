<?php
function fetchURL($urlhash){

	try {
		$db = new SQlite3("db.sqlite3");
		$st = $db->prepare("select * from links where shortcode = :sc limit 1;");
		$st->bindParam(":sc", $urlhash, SQLITE3_TEXT);
		$res = $st->execute();
		$row = $res->fetchArray();
		$db->close();

		// update counter
		$count = 1 + $row[3];
		$shortcode = $row[0];
		$db = new SQlite3("db.sqlite3");
		$st = $db->prepare("update links set viewed = :counter where shortcode = :shortcode;");
		$st->bindParam(":counter", $count, SQLITE3_INTEGER);
		$st->bindParam(":shortcode", $shortcode, SQLITE3_TEXT);
		$res = $st->execute();
		$db->close();

		return $row[1];
	} catch (Exception $ex){
		return 1;
	};
};


$ret = fetchURL(substr($_GET["q"], 0, 128));
if($ret == 1 || $ret == NULL){
	include "errors.php";
	include "includes/footer.html";
	header( "Refresh:3; url=http://" . $_SERVER["HTTP_HOST"] . "/index.php");
	die;
} else {
	printf("redirecting to <a href='%s'>%s</a>", $ret, $ret);
	include "includes/footer.html";
	header( "Refresh:3; url=" . $ret);
	die;
}
?>
